###############################################-----PROYECTO-----FINAL----################################################
##########################################################################################################################
##########################################################################################################################
pack = c("PerformanceAnalytics", "tidyverse","corrplot", "ggplot2","psych","Hmisc","tidyr","tibble","varrank","igraph")

# Instala los paquetes si es que no están instalados
new.packages <- pack[!(pack %in% installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)
#********************************************************************
# 1.2 CARGAR PAQUETES
#********************************************************************
lapply(pack, function(x) require(x, character.only = T))


#########################################MATRIZ DE SPEARMAN#################################
#########################################################################################################
#leer el csv
setwd("correlations_contrast")
bd_pe_at_eco_mx <- read.csv("time-series.csv", header=TRUE)

matriz_pe_at_eco_mx <- as.matrix(bd_pe_at_eco_mx)

#head(matriz_pe_at_eco_mx)
#colnames(matriz_pe_at_eco_mx)

####obtener valores del coeficiente de correlacion de Spearman
matriz_correlacionS <- rcorr(as.matrix(bd_pe_at_eco_mx)[,-1], type=c("spearman"))
matriz_correlacionS
#guardar en un csv la matriz de coeficientes
write.csv(round(matriz_correlacionS$r,2), file="matriz_coeficientes_Spearman_Pesca_Atm_Eco_MX_2021.csv", row.names = F)
#### p-values, significativo? importa?
round(matriz_correlacionS$P,2)
write.csv(round(matriz_correlacionS$P,2), file="matriz_p_values_Pesca_Atm_Eco_MX_2021.csv", row.names = F)

#cargar archivos de las matrices
spearman_cor <- read.csv(file="matriz_coeficientes_Spearman_Pesca_Atm_Eco_MX_2021.csv", header = T)
class(spearman_cor)
#dim(spearman_cor)
#Que onda con la distribución de los datos (histogramas de las variables).
#colnames(bd_enso)=colnames(bd_enso[,-1])
multi.hist(x = matriz_pe_at_eco_mx[,-1], dcol = c("blue", "red"), dlty = c("dotted", "solid"), 
           main = colnames(spearman_cor))

summary(cor(x = bd_pe_at_eco_mx[,-1], method = "spearman"))
dev.off()

#ggpairs() del paquete GGally basada en ggplot2 representa los diagramas de dispersión, 
#el valor de los coeficientes de la correlación e incluso interpreta el tipo de variable
#hacer boxplot de las categóricas 

library(GGally)
#a ver los diagramas de dispersión 
#ggpairs( bd_pe_at_eco_mx[,-1], method="spearman",lower = list(continuous = "smooth"),
#         diag = list(continuous = "bar"), axisLabels = "none",cex.number=.6)
## T_T ## chale...
## poquitas..
ggpairs( bd_pe_at_eco_mx[17:30], method="spearman",lower = list(continuous = "smooth"),
         diag = list(continuous = "bar"), axisLabels = "none",cex.number=.1)

#visualizacion matriz spearman
m_spearman <- as.matrix(spearman_cor)
col <- colorRampPalette(c("#BB4444","#EE9988","#FFFFFF","#77AADD","#4477AA"))
corrplot(m_spearman,method="square",
         tl.col="black",
         tl.srt= 35,
         tl.cex = 0.5,
         col=col(200),
         addCoef.col="black",
         order="AOE",
         type="upper",
         diaf=F,
         addshade="all",
         number.digits=1,
         number.cex=0.6,
         main="Matriz de correlacion, Spearman")

corrplot(cor(x=bd_pe_at_eco_mx[-1],method="spearman"), method="number", number.cex=0.3, addCoef.col = "black", tl.cex = 0.5, tl.col = "black",tl.srt=35, main="Matriz de correlacion ENSO")

##########################################################################################################
###########################################################################################################
###########################################################################################################
###########################################################################################################
###########################################################################################################
##################################-----REDES-----SPEARMAN------PE-ATM-ECO-MX ----##########################
###########################################################################################################
###########################################################################################################
###########################################################################################################
###########################################################################################################
class(m_spearman)
#red lineal Spearman
grafo_spearman <- simplify(graph.adjacency(m_spearman,
                                       mode="undirected",
                                      weighted=TRUE,diag=F),remove.loops = T,
                                         remove.multiple = T)
  

plot(grafo_spearman, edge.width = E(grafo_spearman)$weight)
V(grafo_spearman)$name

summary(grafo_spearman)
edge.attributes(grafo_spearman, index(m_spearman))

#Caracterizar la red de C. Spearman con distinción de coeficientes - y coeficientes + 
#sin cut off
#azul positivo, rojo negativo
g1_rlineal<- plot(grafo_spearman,
                  mode="undirected",
                  weighted=TRUE,diag=F,
                  edge.width=abs(E(grafo_spearman)$weight)*6,
                  vertex.label.color="black",
                  cex=0.2,
                  vertex.color="grey",
                  vertex.shape="sphere",
                  edge.width=abs(E(grafo_spearman)$weight)*4,
                  edge.color=ifelse(m_spearman > 0, "red","blue"),
                  main="Red C. Spearman",
                  layout=layout_in_circle)


#selección de valores con correlación media (osea > 0.3)
dg12=delete.edges(grafo_spearman,which(E(grafo_spearman)$weight < 0.3))
E(dg12)$weight
a<- E(dg12)$weight

#selección de valores negativos >-0.3
dgn12=delete.edges(grafo_spearman,which(E(grafo_spearman)$weight > -0.3))
dgn12
E(dgn12)$weight
b<-(abs(E(dgn12)$weight))
b
abs_cor <- abs(m_spearman)
abs_cor

E(grafo_spearman)$weight
plot(grafo_spearman, vertex.size=10, 
     edge.width=abs_cor*6,
     vertex.shapes="sphere",
     vertex.label.cex=0.8,
     vertex.label.dist=0.5,
     vertex.color="slategray1",
     vertex.label.color="black",
     layout=layout_in_circle,
     main="Red C. Spearman",
     edge.label=E(dg12)$weight,
     edge.label.color=ifelse(E(grafo_spearman)$weight > 0,"blue","red"),
     #hacer aquí un if de selección si es a rojo si es b azul
     edge.color=ifelse(E(grafo_spearman)$weight > 0,"blue","red"))

#los nodos se conectan sí y sólo sí el coeficiente es >=+-0.3
##############################################################
############# RED SPEARMAN CON REGLA DE CORRESPONDENCIA ###################

gs <- delete.edges(grafo_spearman, E(grafo_spearman)[ abs(weight) < 0.3 ])
gs
summary(gs)
vertex_attr(gs)
edge_attr(gs)
table(E(gs)$weight)

## un layout bien feo T_T ##
plot(gs,
     edge.width=abs_cor*6,
     vertex.shapes="sphere",
     vertex.label.cex=0.8,
     vertex.label.dist=0.5,
     vertex.color="slategray1",
     vertex.label.color="black",
     layout=layout_as_tree,
     main="Red C. Spearman",
     edge.label=E(gs)$weight,
     edge.label.color=ifelse(E(gs)$weight > 0, "blue","red"),
     edge.color=ifelse(E(gs)$weight > 0,"blue","red"),
     edge.color=ifelse(round(matriz_correlacionS$r,2) > 0, "blue","red"))


# círculo
plot(gs,
     edge.width=abs_cor*6,
     vertex.shapes="sphere",
     vertex.label.cex=0.8,
     vertex.label.dist=0.5,
     vertex.color="slategray1",
     vertex.label.color="black",
     layout=layout_in_circle,
     main="Red C. Spearman",
     edge.label=NA,
     edge.color=ifelse(E(gs)$weight > 0,"blue","red"),
     edge.color=ifelse(round(matriz_correlacionS$r,2) > 0, "blue","red"))

#arreglo en círuclo con nombres en los enlaces
plot(gs,
     edge.width=abs_cor*6,
     vertex.shapes="sphere",
     vertex.label.cex=0.8,
     vertex.label.dist=0.5,
     vertex.color="slategray1",
     vertex.label.color="black",
     layout=layout_in_circle,
     main="Red C. Spearman",
     edge.label=E(gs)$weight,
     edge.color=ifelse(E(gs)$weight > 0,"blue","red"),
     edge.color=ifelse(round(matriz_correlacionS$r,2) > 0, "blue","red"))

#agregar pesos y luego plot
#red sin v negativos
E(grafo_spearman)$weight
#red con valores negativos
E(gs)$weight

table(abs(E(gs)$weight))
table(E(gs)$weight)
#write.csv(table(E(gs)$weight),"tabla de pesos_Spearman_conectados_pe_atm_eco_mx_2021.csv")
frec_corr_l <- read.csv("tabla de pesos_Spearman_conectados_pe_atm_eco_mx_2021.csv", header=T)
frec_corr_l
hist(x=frec_corr_l[,-1]$Var1,main = "Histograma de F. C. Spearman", xlab = "C.Spearman", ylab = "Frecuencia",labels=T)

##acá algo bien raro... ¬ ¬!
max(E(gs)$weight)
##############################################################
##############################################################
##############################################################
##############################################################
##############################################################
##############################################################
##############   medidas de centralidad     ##################

#grado
grado_red_spearman <- degree(gs)
grado_red_spearman
#view(grado_red_spearman)
#write.csv(grado_red_spearman, file="grado_red_spearman_pe_atm_eco_mex_2021.csv", row.names = T)
max(grado_red_spearman);min(grado_red_spearman)
summary(grado_red_spearman)

# centralidad de eigenvector
abs(E(gs)$weight)
eig_red_spearman <- eigen_centrality(gs)
eig_red_spearman[[1]]
tabla_eig<- as.data.frame(eig_red_spearman[[1]])
#guardar la tablita
#write.csv(tabla_eig, file="centralidad_eigen_pe_atm_eco_mex_2021.csv", row.names = T)


#centralidad de edge.betweennes
enlaces_bs<-(edge.betweenness(gs,e=abs(E(gs,directed=F)),
                             weights=abs(E(gs)$weight)))

max(enlaces_bs)
ebig<-max(enlaces_bs)
#write.csv(enlaces_bs, file="centralidad_edge_betweennes_pe_atm_eco_mex_2021.csv", row.names=T)
wc<- walktrap.community(gs)
gs
plot(wc,gs,
     edge.width=(enlaces_bs/ebig)*10,
     layout=layout.fruchterman.reingold(gs))




#el tamaño del enlace corresponde al coeficiente de Spearman 
#help(layout.fruchterman.reingold)     
#help(edge.betweenness)

######  centralidad de eigenvector SPEARMAN
eig_glineal <- eigen_centrality(gs, directed=F, scale=T)
eig_glineal[[1]]
#write.csv(eig_glineal[[1]], file="ceigenvector_pe_atm_eco_mex_2021.csv", row.names = F)
E(gs)$weight
E(gs)$width <- abs((E(gs)$weight))/5
E(gs)$width
wc<- walktrap.community(gs)
plot(wc,gs,
     edge.width=(enlaces_bs/ebig)*5,
     layout=layout.fruchterman.reingold(gs),
     vertex.label.cex=0.6,
     vertex.label.color="black")

plot(wc,gs,
     edge.width=(enlace_cerc_gs/ebig)*5,
     vertex.label.cex=0.6,
     vertex.label.color="black",
     layout=layout_with_dh)


#Clustering
#La transitividad global de una red directa o indirecta son triangulitos.
transitivity(gs)

#detección de comunidades
gs_wt <- walktrap.community(gs,
                            steps=50,
                            modularity=TRUE)
print(gs_wt)
#view(gs_wt)
comunidades_gs <- edge.betweenness.community(gs,e=abs(E(gs,directed=F)),
                                                weights=(abs(E(gs)$weight)),
                                                merges=T,
                                                bridges=T)
comunidades_gs$modularity
#view(comunidades_gs$modularity)
#print(comunidades_gs$modularity)
write.csv(comunidades_gs$modularity, file="tabla_comunidades_pe_atm_eco_mex_2021.csv",row.names=T)
max(comunidades_gs$modularity); min(comunidades_gs$modularity)

plot(as.dendrogram(comunidades_gs))



# closeness
#si los coeficientes son negativos no es buena idea pero...
abs(E(gs)$weight)
edge.width=abs(E(gs)$weight)*4

grafo_spearman_2<- simplify(grafo_spearman,
                             remove.loops = T,
                             remove.multiple = T)

matriz_g<- as.matrix(grafo_spearman)
matriz_g

#no se recomienda, altera la topología de la red, hacerlo sólo para fines visuales 

#cut.off <- 0.3
#grafo_spearman.cut<-delete_edges(grafo_spearman,
# E(grafo_spearman)[weight<cut.off])
#plot(grafo_spearman<-delete_edges(grafo_spearman,
#  E(grafo_spearman)[weight<cut.off]))
#plot(grafo_spearman,edge.width=E(grafo_spearman)$width)


install.packages("ggraph")
#install.packages("tidygraph")
library(ggraph)
#library(tidygraph)

## redes con arcos azules y nodos grises
ggraph(grafo_spearman, layout = 'linear') + 
  geom_edge_arc(color = "#00aaff", width=0.4) +
  geom_node_point(size=5, color="gray50") +
  theme_void()

grafo_spearman %>%
  ggraph(layout = "linear") +
  geom_edge_link() +
  geom_node_label(aes(label = name)) +
  theme_graph()

grafo_spearman%>%
  ggraph(layout = "linear") +
  geom_edge_arc() +
  geom_node_label(aes(label = name)) +
  theme_graph()

#install.packages("rglplot")
#library(rglplot)
#install.packages("rgl")
#library(rgl)
#coords <- layout_with_fr(grafo_spearman, dim=3)
#rglplot(grafo_spearman, layout=coords)

library(visNetwork)
install.packages("geomnet")
library(geomnet)

#Create graph for Louvain
graph <- graph_from_data_frame(spearman_cor, directed = FALSE)
plot(graph)
#Louvain Comunity Detection
cluster <- cluster_louvain(graph)

cluster_df <- data.frame(as.list(membership(cluster)))
cluster_df <- as.data.frame(t(cluster_df))
cluster_df$label <- rownames(cluster_df)

#Create group column
nodes <- left_join(Atun, cluster_df, by = "label")
colnames(Atun)[3] <- "group"


install.packages("threejs")
install.packages("htmlwidgets")
detach("package:igraph")
detach("package:igraph")
library(threejs)
library(htmlwidgets)


net.js <- grafo_spearman
graph_attr(net.js, "layout") <- NULL
gjs <- graphjs(net.js, main="Red pesca-atm-eco-MX!", bg="gray10", showLabels=F, stroke=F, 
               curvature=0.6, attraction=0.2, repulsion=0.8, opacity=0.9)
print(gjs)
#saveWidget(gjs, file="Media-Network_pe_atm_eco_mx_2021-gjs.html")
browseURL("Media-Network:pe_atm_eco_mx_2021-gjs.html")



deg.dist.gs <- degree.distribution(gs)
plot(deg.dist.gs, xlab="k", ylab="P(k)", main="Scale-free network")
degree.distribution(gs)
dist_grado_spearman <- degree.distribution(gs)


fit.scale.free_sp <- power.law.fit(dist_grado_spearman )
fit.scale.free_sp[degree.distribution(gs)]

deg.dist.fyi <- degree.distribution(grafo_spearman)
deg.dist.fyi

plot(deg.dist.fyi, xlab="k", ylab="P(k)", main="Scale-free network")
#par(mfrow=c(1,2))

#K-core decomposition
#The k-core is the maximal subgraph in which every node has degree of at least k.
#The result here gives the coreness of each vertex in the network. 
#A node has coreness D if it belongs to a D-core but not to (D+1)-core.
kc <- coreness(grafo_spearman, mode="all")
plot(grafo_spearman, vertex.size=kc*0.21, vertex.label=kc, vertex.color=colrs[kc])


average.path.length(grafo_spearman)
diameter(grafo_spearman)
crp <- clusters(grafo_spearman)
crp
clique_num(grafo_spearman)


# propiamente las diadas tambien son cliques 
# sin embargo vamos a considerar  subredes
# a partir de  3 nodos
max_cliques(grafo_spearman, min=3)


#RED LIBRE DE ESCALA
data.dist <- data.frame(k=0:max(m_spearman),p_k=degree_distribution(grafo_spearman))
data.dist1 <- data.dist[data.dist$p_k>0,]

#grafiquito sin nodos
plot(grafo_spearman, 
     vertex.shape="none", 
     vertex.label=V(grafo_spearman)$media,vertex.label.font=2, 
     vertex.label.color="gray40",
     vertex.label.cex=.7, 
     edge.color="gray85")

#grafiquito de comparación de modelos de redes
###GRafos de distribui?n de grado
erdosgraph <- erdos.renyi.game(500, 350, type = "gnm")
strogatzgraph<- watts.strogatz.game(1, 500, 1, 0.35, loops = FALSE, multiple = FALSE)
#mi red
gs
plot(degree.distribution(strogatzgraph),type="b",pch=1,col="blue",
     xlab="Degree", ylab="Frequency")
points(degree.distribution(erdosgraph),type="b",pch=2,col="red")
points(degree.distribution(grafo_spearman),type="b",pch=3,col="green")
points(degree.distribution(gs),type="b",pch=4,col="purple")
legend("topright", c("strogatz","erdos","C.Spearman","IM"), col=c("blue","red","green","purple"), pch=c(1,2,3,4),
       ncol=1, yjust=0, lty=0)


#red Spearman
grafo_spearman2 <- simplify(graph.adjacency(m_spearman,
                                           mode="directed",
                                           weighted=TRUE,diag=F),remove.loops = T,
                           remove.multiple = T)

plot(grafo_spearman2)
ggraph(grafo_spearman2, 'dendrogram') + 
  geom_edge_diagonal()

dendrogram_spearman <- as.dendrogram(hclust(dist(grafo_spearman2[, 1:10])))
ggraph(dendrogram_spearman, 'dendrogram') + 
  geom_edge_elbow()

ggraph(dendrogram_spearman, 'dendrogram', circular = TRUE) + 
  geom_edge_elbow() + 
  coord_fixed()

news.path <-shortest_paths(grafo_spearman,from =V(grafo_spearman)[crp=="MSNBC"],
                           to  =V(grafo_spearman)[crp=="New York Post"],
                           output = "both")# both path nodes and edges# Generate edge color variable to plot the path:ecol <-rep("gray80",ecount(net))ecol[unlist(news.path$epath)] <- "orange"# Generate edge width variable to plot the path:ew <-rep(2,ecount(net))ew[unlist(news.path$epath)] <- 4# Generate node color variable to plot the path:vcol <-rep("gray40",vcount(net))vcol[unlist(news.path$vpath)] <- "gold"plot(net, vertex.color=vcol, edge.color=ecol,edge.width=ew, edge.arrow.mode=0)

rn.neigh = connect.neighborhood(grafo_spearman, 5)

plot(rn.neigh, vertex.size=8, vertex.label=NA) 
deg.dist <- degree_distribution(grafo_spearman, cumulative=T, mode="all")

plot( x=0:max(deg), y=1-deg.dist, pch=19, cex=1.2, col="orange", 
      
      xlab="Degree", ylab="Cumulative Frequency")

cl <- clusters(gs)
cl
plot(gs, layout=layout, vertex.weight=cl$csize+1L)

#v mas importantes #chequear nodos > grado
###################clustering
#presentacion ---> histogramas
#grafos mostrar > 
#grado,clustering , comunidades
#?cntralidad + fuerte
#centralidad de > a < todas
#eigenvector de > a >
#explicacion hist
#tipo de red
#background, por que esas variables
#dif entre corr lineal y no lineal
#resultados interpretacion de lineal y nol ineal
#dif entre red
#que implican estas  relaciones, si estan corr las pesque, si me llevo atun, me llevo todas las dem?s
#buscar medidas inferidas estadisticamnte
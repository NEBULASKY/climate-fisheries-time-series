import csv
from visibility_graph import visibility_graph
from slugify import slugify
from os import path
import networkx as nx
from basic_stats import basic_stats
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt

with open("pesquerias_series.csv") as f:
    db = csv.DictReader(f)

    data = {}
    for field in db.fieldnames:
        series = [float(l[field])
                  for l in db
                  if l[field] != field]
        g = visibility_graph(series)

        data[field] = basic_stats(g)
        
        with open(path.join('graphs',
                            slugify(field))+'.gpickle',
                  'wb') as p:
            nx.write_gpickle(g, p)
            nx.write_graphml(g, path.join('graphs',
                                          slugify(field))+'.graphml')

        #print(slugify(field))

        f.seek(0)
del(data['AÑO'])
        
df = pd.DataFrame.from_dict(data, orient='index')

normalized_df=(df-df.min())/(df.max()-df.min())

normalized_df['index1'] = normalized_df.index

fig, ax = plt.subplots()
fig.tight_layout()


pd.plotting.parallel_coordinates(
    normalized_df, class_column='index1'
)

legend = ax.legend()
legend.remove()

fig.savefig('parallel_coordinates_visibility_graph_struct.png', dpi=300)

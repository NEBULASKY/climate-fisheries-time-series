
import networkx as nx

def basic_stats(G):

    degrees = []
    for i,c in enumerate(nx.degree_histogram(G)):
        degrees += c * [i, ]
    K = sum(degrees)/len(degrees)

    return {
        # 'nodes': nx.number_of_nodes(G),
        'edges': nx.number_of_edges(G),
        'avg_k': K,
        'spl': nx.average_shortest_path_length(G),
        'radius': nx.algorithms.distance_measures.radius(G, e=None, usebounds=False),
        'diameter': nx.algorithms.distance_measures.diameter(G, e=None, usebounds=False),
        'density': nx.density(G),
        'clustering': nx.average_clustering(G),
    }


